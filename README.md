#### StudentRecord

- Using C#/.NET CORE & Sqlite

- Some FYI, scaffoled up some of the boilerplate controller/view code and made adjustments
    - Student Controller - updated use of `_context` to `_repo`
    - Students/Create View updated `asp-validation-summary` value from `ModelOnly` to `All` in order to display validation errors for child class validations.

#### Pertinent Commits

    You can leave comments, insults, funny gifs, etc... on whichever line jumps out at you.

   - [Repo Pattern](https://bitbucket.org/alejandroereyes/studentrecord/commits/35f748e1b2a950e4075391a2ba7ee27f45948d49)
   - [Models & Interface](https://bitbucket.org/alejandroereyes/studentrecord/commits/90d07cb8cd91639188d692858646b07af7f834ae)
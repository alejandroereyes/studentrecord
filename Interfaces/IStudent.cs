﻿using System;
namespace StudentRecord.Interfaces
{
    public interface IStudent
    {
        string BuyCafetariaCredit(double amount);
        string RegisterForClass(int id);
        string Transcript();
    }
}

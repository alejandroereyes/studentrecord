﻿
using System;
using StudentRecord.Interfaces;

namespace StudentRecord.Models
{
    public class Graduate : Student, IStudent
    {
        public string BuyCafetariaCredit(double amount)
        {
            return $"Graduate cafe credit to ${amount}";
        }
        public string RegisterForClass(int id)
        {
            return $"Graduate registered for class {id}";
        }
        public string Transcript()
        {
            return "Graduate transcript";
        }
    }
}
﻿using System;
using StudentRecord.Interfaces;

namespace StudentRecord.Models
{
    public class Doctorate : Student, IStudent
    {
        public string BuyCafetariaCredit(double amount)
        {
            return $"Doctorate cafe credit to ${amount}";
        }
        public string RegisterForClass(int id)
        {
            return $"Doctorate registered for class {id}";
        }
        public string Transcript()
        {
            return "Doctorate transcript";
        }
    }
}
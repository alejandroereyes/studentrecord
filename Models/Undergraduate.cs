﻿using System;
using StudentRecord.Interfaces;

namespace StudentRecord.Models
{
    public class Undergraduate : Student, IStudent
    {
        public string BuyCafetariaCredit(double amount)
        {
            return $"Undergrad cafe credit to ${amount}";
        }
        public string RegisterForClass(int id)
        {
            return $"Undergrad registered for class {id}";
        }
        public string Transcript()
        {
            return "Undergrad transcript";
        }
    }
}

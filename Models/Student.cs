﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
// using System.linq achieves the same behavoir,
// but it was fun to play with adding an extention method.
using LessSucky;
namespace LessSucky
{
    public static class CSharpArray
    {
        public static bool Contains<T>(this T[] arr, dynamic item)
        {
            return Array.Exists(arr, el => el == item);
        }
    }
}

namespace StudentRecord.Models
{
    public class Student : Account, IValidatableObject
    {
        [Required]
        public virtual string DegreeTrack { get; set; }

        public override IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
	    {
            if (IsAdmin)
	        {
                yield return new ValidationResult("Student cannot be an admin", new[]  { "IsAdmin", "false" });
	        }
            if (!ValidDegreeTracks.Contains(DegreeTrack))
            {
                yield return new ValidationResult($"{DegreeTrack} is invalid. {String.Join(", ", ValidDegreeTracks)} are valid tracks.");
            }
	        else
	        {
                foreach(var result in base.Validate(validationContext))
                {
                    yield return result;
                }
	        }
	    }

        // TODO: find a better way to represent fixed valid degree tracks
        protected static readonly string UndergradStudent = "Undergraduate";
        protected static readonly string GradStudent = "Graduate";
        protected static readonly string Doctoral = "Doctorate";
        protected static readonly string[] ValidDegreeTracks = new string[] { UndergradStudent, GradStudent, Doctoral };
    }
}

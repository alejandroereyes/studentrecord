﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace StudentRecord.Models.Query
{
    public static class RepoQuery
    {
        public static Task<T> FindById<T>(DbSet<T> repoSet, int id) where T : EntityBase
        {
            return repoSet.SingleOrDefaultAsync(record => record.ID == id);
        }

        public static bool Exists<T>(DbSet<T> repoSet, int id) where T : EntityBase
        {
            return repoSet.Any(record => record.ID == id);
        }
    }
}

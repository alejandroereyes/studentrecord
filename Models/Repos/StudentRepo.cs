﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using StudentRecord.Models.Query;

namespace StudentRecord.Models.Repos
{
    public class StudentRepo : RepoBase<StudentRepo>
    {
        public StudentRepo(StudentRecordContext dbContext) : base(dbContext)
        {
        }

        public DbSet<Student> All()
        {
            return _context.Students;
        }

        public dynamic FindById(int id)
        {
            return RepoQuery.FindById<Student>(All(), id);
        }

        public bool Exists(int id)
        {
            return RepoQuery.Exists<Student>(All(), id);
        }
    }
}
﻿using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using StudentRecord.Models.Query;
using System.Threading.Tasks;

namespace StudentRecord.Models.Repos
{
    public class RepoBase<T>
    {
        public RepoBase(StudentRecordContext dbContext)
        {
            _context = dbContext;
        }

        public Task Create(EntityBase entity)
        {
            _context.Add(entity);
            return SaveAsync();
        }

        public Task Update(EntityBase entity)
        {
            _context.Update(entity);
            return SaveAsync();
        }

        public Task Remove(EntityBase entity)
        {
            _context.Remove(entity);
            return SaveAsync();
        }

        protected Task SaveAsync()
        {
            return _context.SaveChangesAsync();    
        }

        protected StudentRecordContext _context;
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace StudentRecord.Models
{
    public class DBinitialize
    {
        public static void EnsureCreated(IServiceProvider serviceProvider)
        {
            var context = new StudentRecordContext(
                serviceProvider.GetRequiredService<DbContextOptions<StudentRecordContext>>());
            context.Database.EnsureCreated();
        }
    }
}

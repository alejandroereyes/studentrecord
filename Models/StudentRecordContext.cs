﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.EntityFrameworkCore;

namespace StudentRecord.Models
{
    public class StudentRecordContext : DbContext
    {
        public static DbContextOptions<StudentRecordContext> CurrentOptions;
        public StudentRecordContext(DbContextOptions<StudentRecordContext> options)
            :base(options)
        {
            StudentRecordContext.CurrentOptions = options;
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<Student> Students { get; set; }
    }
}

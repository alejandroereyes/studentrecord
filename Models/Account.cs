﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace StudentRecord.Models
{
    [Table("Accounts")]
    public class Account : EntityBase, IValidatableObject
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public int PhoneNumber { get; set; } 

        [Required]
        public string Address { get; set; }

        [Required]
        public bool IsAdmin { get; set; }
    
        public virtual IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (!IsPhoneNumberValid())
            {
                yield return new ValidationResult
                    ("Phone Number must be 10 digits long", new[] { "PhoneNumber", "0000000000" });
            }
        }

        private bool IsPhoneNumberValid()
        {
            return PhoneNumber.ToString().Length == REQUIRED_PHONE_NUM_LENGTH;
        }
        private const int REQUIRED_PHONE_NUM_LENGTH = 10;
    }
}
